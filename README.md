# zh-cn_localization

#### 介绍
Industries Of Titan [泰坦工业] 中文本地化 

支持Epic    支持当前版本0.13.0  或更高版本

这是泰坦工业的非官方中文本地化    如遇到翻译不准确欢迎提议
目前正在不定时的翻译

文件所在位置:
\IndustriesOfTitan\Titan\Content\Localization\BraceYourselfGames

如何使用文件
1.  下载的本地化文件
2.  打开\IndustriesOfTitan\Titan\Content\Localization\BraceYourselfGames目录
3.  将本地化的loc_zh-CN.csv(官方提取文件 loc_zh_HANS.csv)复制到BraceYourselfGames目录中，无需重写任何其他文件
4.   _如果更新翻译，只需覆盖 loc_zh-CN.csv_ 
5.  运行游戏
6.  找到Settings -> Gameplay -> Language->选择中文->点击确认   
    或 主界面右下角选择中文
7.  重新启动游戏

